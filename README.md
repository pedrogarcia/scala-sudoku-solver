ScalaSudokuSolver
=================


Description
-----------

Scala Sudoku Solver (3S) is a little program to solve sudoku puzzles.


License
-------

Scala Sudoku Solver is released under [GPL version 2][0].


Requeriments
------------

Scala Sudoku Solver requires JVM 1.6 (or superior) installed on your system.


Download and Install
--------------------

To run the program, download the [Sudoku.jar][1] and run:

> `$ wget http://www.sawp.com.br/projects/scalasudokusolver/files/Sudoku.jar`  
> `$ java -jar Sudoku.jar`
> 

How to Use
----------

1. Open the program (`java -jar Sudoku.jar`)
2. Fill with initial numbers and click in 'solve' button:
3. The program will solve the puzzle.


How to contribute
-----------------

If you want to contribute to this project, you can report bugs by mail
**sawp@sawp.com.br**



[0]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
[1]: http://www.sawp.com.br/projects/scalasudokusolver/files/Sudoku.jar
[2]: http://bitbucket.org/kuraiev/scalasudokusolver
