/**
 * User: pedro
 * Date: 06/03/13
 * Time: 14:50
 */
package br.com.sawp.sss.solver

import scala.Array
import br.com.sawp.sss.types.Types._
import br.com.sawp.sss.general.StringFunctions

class SudokuSolver(rowData: SquareAny) {
  val board = rowData map {
    x =>
      x map {
        y => StringFunctions.convertToInt(StringFunctions.convertSpacesToZero(y))
      }
  }

  private def isValidMove(cell: SquareInt, i: Int, j: Int, v: Int): Boolean = {
    val range = 0 until 9
    for (k <- range if v == cell(k)(j))
      return false
    for (k <- range if v == cell(i)(k))
      return false
    val boxX = (i / 3) * 3
    val boxY = (j / 3) * 3
    for (k <- 0 until 3;
         m <- 0 until 3
         if v == cell(boxX + k)(boxY + m))
      return false
    return true
  }

  private def check(x: Int, y: Int, acc: SquareInt): Boolean = {
    var row = x
    var col = y
    if (row == 9) {
      row = 0
      col += 1
      if (col == 9) {
        return true
      }
    }
    if (acc(row)(col) != 0)
      return check(row + 1, col, acc)

    for (v <- 1 to 9 if isValidMove(acc, row, col, v)) {
      acc(row)(col) = v
      if (check(row + 1, col, acc))
        return true
    }
    acc(row)(col) = 0
    return false
  }

  def solve: SquareInt = {
    check(0, 0, board)
    board
  }
}

object SudokuSolver {
  def solve(in: SquareAny): SquareInt = {
    val solver = new SudokuSolver(in)
    solver.solve
  }

  def isValidInitialPositions(rowData: SquareAny): Boolean = {
    val model = rowData map {
      x =>
        x map {
          y => StringFunctions.convertToInt(StringFunctions.convertSpacesToZero(y))
        }
    }

    def checkRow(row: Int, num: Int): Boolean = {
      val check = for (col <- 0 until 9) yield model(row)(col)
      val occurrences = check.count(n => n == num)
      occurrences <= 1
    }

    def checkCol(col: Int, num: Int): Boolean = {
      val check = for (row <- 0 until 9) yield model(row)(col)
      val occurrences = check.count(n => n == num)
      occurrences <= 1
    }

    def checkBox(row: Int, col: Int, num: Int): Boolean = {
      val (r, c) = (row / 3 * 3, col / 3 * 3)
      val check =
        for {lin <- 0 until 3
             column <- 0 until 3} yield model(r + lin)(c + column)
      val occurrences = check.count(n => n == num)
      occurrences <= 1
    }

    val checkAllBoard =
      for {i <- 0 until 9
           j <- 0 until 9
           k <- 1 to 9} yield checkRow(i, k) && checkCol(j, k) && checkBox(i, j, k)
    val occurrencesOfRepetition = checkAllBoard.count(x => x == false)
    occurrencesOfRepetition == 0
  }

  def isValidRangeInput(rowData: SquareAny): Boolean = {
    val flattened = flattenArray(rowData)
    val model = flattened map {
      x => StringFunctions.convertToInt(StringFunctions.convertSpacesToZero(x))
    }
    val checkBoard = model map {
      y => y >= 0 && y <= 9
    }
    val occurrencesOfRepetition = checkBoard.count(x => x == false)
    occurrencesOfRepetition == 0
  }

  def isValidNumberOfInputs(rowData: SquareAny): Boolean = {
    val flattened = flattenArray(rowData)
    val integerValues = flattened map {
      x => StringFunctions.convertToInt(StringFunctions.convertSpacesToZero(x))
    }
    val zeros = integerValues map {
      x => if (x != 0) 1 else 0
    }
    zeros.sum >= 32
  }

  private def flattenArray(arr: SquareAny): Array[Any] = {
    val ret = for (i <- 0 until 9;
                   j <- 0 until 9) yield arr(i)(j)
    ret.toArray
  }
}
