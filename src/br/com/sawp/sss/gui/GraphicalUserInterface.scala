package br.com.sawp.sss.gui

import java.awt.Color
import javax.swing.BorderFactory
import swing.Dialog.Message
import swing.Swing.EmptyIcon
import swing._
import event.ButtonClicked
import br.com.sawp.sss.solver.SudokuSolver

/**
 * User: Pedro Garcia
 * Date: 3/14/13
 * Time: 4:49 PM
 */
object GraphicalUserInterface {
  private val headers = Seq.range(0, 9)
  private var rowData = Array.tabulate[Any](9, 9) {
    (x, y) => " "
  }

  private val solveButton = new Button {
    text = "Solve sudoku"
  }
  private val clearButton = new Button {
    text = "Clear"
  }
  private val aboutButton = new Button {
    text = "About"
  }
  private val closeButton = new Button() {
    text = "Exit"
  }

  private val table = new Table(rowData, headers) {
    rowHeight = 50
    peer.setGridColor(Color.GRAY)
    peer.setBorder(BorderFactory.createLineBorder(Color.BLACK))

    override def rendererComponent(isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component = {
      if (hasFocus)
        new TextField(rowData(row)(column).toString) {
          horizontalAlignment = Alignment.Center
        }
      else
        new Label(rowData(row)(column).toString) {
          if (row % 3 == 0)
            peer.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.BLACK))
          if (column % 3 == 0)
            peer.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.BLACK))
          if (row % 3 == 0 && column % 3 == 0)
            peer.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 0, Color.BLACK))
          xAlignment = Alignment.Center
        }
    }
  }

  lazy val mainPanel = new BoxPanel(Orientation.Vertical) {
    listenTo(table.selection)
    listenTo(solveButton, clearButton, aboutButton, closeButton)

    reactions += {
      case ButtonClicked(`solveButton`) =>
        ButtonActions.solveButtonClicked
      case ButtonClicked(`clearButton`) =>
        ButtonActions.clearButtonClicked
      case ButtonClicked(`aboutButton`) =>
        ButtonActions.aboutButtonClicked
      case ButtonClicked(`closeButton`) =>
        ButtonActions.closeButtonClicked
    }

    contents += table
    contents += new BoxPanel(Orientation.Horizontal) {
      contents += new ScrollPane(solveButton)
      contents += new ScrollPane(clearButton)
      contents += new ScrollPane(aboutButton)
      contents += new ScrollPane(closeButton)
    }
  }

  private object ButtonActions {
    def closeButtonClicked {
      sys.exit
    }

    def solveButtonClicked {
      table.repaint
      val isValidInitialPositions = SudokuSolver.isValidInitialPositions(rowData)
      val isValidNumberOfInputs = SudokuSolver.isValidNumberOfInputs(rowData)
      val isValidRangeInput = SudokuSolver.isValidRangeInput(rowData)
      if (isValidNumberOfInputs && isValidInitialPositions && isValidRangeInput) {
        val answer = SudokuSolver.solve(rowData.clone)
        for (i <- 0 until 9; j <- 0 until 9)
          rowData(i)(j) = answer(i)(j)
        table.repaint
      } else {
        if (!isValidInitialPositions)
          Dialog.showMessage(
            parent = null,
            message = "Wrong initial values.\nCheck if there are repeated numbers in the rows, columns or boxes.",
            title = "Few Values Warning",
            messageType = Message.Warning,
            icon = EmptyIcon
          )
        if (!isValidNumberOfInputs)
          Dialog.showMessage(
            parent = null,
            message = "Not enough values. At least 32 values must be entered.",
            title = "Few Values Warning",
            messageType = Message.Warning,
            icon = EmptyIcon
          )
        if (!isValidRangeInput)
          Dialog.showMessage(
            parent = null,
            message = "Wrong values.\nIn sudoku game all numbers must be contained in interval [1,9].",
            title = "Few Values Warning",
            messageType = Message.Warning,
            icon = EmptyIcon
          )
      }
    }

    def clearButtonClicked {
      for (i <- 0 until 9; j <- 0 until 9)
        rowData(i)(j) = " "
      table.repaint
    }

    def aboutButtonClicked {
      Dialog.showMessage(
        parent = null,
        message = "Pedro Garcia <sawp@sawp.com.br>\n",
        title = "Contact",
        messageType = Message.Info,
        icon = EmptyIcon
      )
    }
  }
}
