/**
 * User: pedro
 * Date: 28/02/13
 * Time: 17:41
 */
package br.com.sawp.sss.general

import swing._
import javax.swing.UIManager
import br.com.sawp.sss.gui.GraphicalUserInterface

object Main extends SimpleSwingApplication {
  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName)

  def top = new MainFrame {
    title = "Scala Sudoku Solver (SSS)"
    preferredSize = new Dimension(450, 550)
    resizable = false
    contents = GraphicalUserInterface.mainPanel
  }
}