/**
 * Created with IntelliJ IDEA.
 * User: pedro
 * Date: 08/03/13
 * Time: 11:03
 * To change this template use File | Settings | File Templates.
 */
package br.com.sawp.sss.general

object StringFunctions   {
  def isValidNumeric(input: String): Boolean = input.trim.forall(_.isDigit) && !input.trim.isEmpty

  def convertSpacesToZero[T](char: T): Int = char match {
    case c: Char => convertToInt(c)
    case s: String => {
      if (isValidNumeric(s)) convertToInt(s.trim) else 0
    }
    case any: Any => any.asInstanceOf[Int]
  }

  def convertToInt[T](value: T): Int = value match {
    case char: Char => char.toInt
    case string: String => string.toInt
    case integer: Int => integer
    case any: Any => any.asInstanceOf[Int]
  }
}
