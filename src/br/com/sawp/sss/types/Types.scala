package br.com.sawp.sss.types

import collection.mutable.ArrayBuffer

/**
 * User: Pedro Garcia
 * Date: 3/14/13
 * Time: 4:51 PM
 */
object Types {
  type SquareInt = Array[Array[Int]]
  type SquareAny = Array[Array[Any]]
  type CubeInt = ArrayBuffer[ArrayBuffer[ArrayBuffer[Int]]]
}
